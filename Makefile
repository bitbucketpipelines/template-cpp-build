# Basic Makefile that allows to build, test, lint, compile, distcheck and clean.
# For best practise create Makefile in every project directory that needs to be executed or compiled

compile:
	mkdir -p bin && g++ src/main.cpp -o bin/template

distcheck:
	./bin/template

check:
	cd test && make check

lint:
	apt update && apt -y install cppcheck && cppcheck .
