#include <iostream>

const char* greetings() {
    return "Hello from Bitbucket Pipes Templates! ";
}

int main() {
    std::cout << greetings();
}
